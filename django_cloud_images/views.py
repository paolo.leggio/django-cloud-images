# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    UpdateView,
    ListView
)

from .models import (
	Image,
	Collection,
)


class ImageCreateView(CreateView):

    model = Image


class ImageDeleteView(DeleteView):

    model = Image


class ImageDetailView(DetailView):

    model = Image


class ImageUpdateView(UpdateView):

    model = Image


class ImageListView(ListView):

    model = Image


class CollectionCreateView(CreateView):

    model = Collection


class CollectionDeleteView(DeleteView):

    model = Collection


class CollectionDetailView(DetailView):

    model = Collection


class CollectionUpdateView(UpdateView):

    model = Collection


class CollectionListView(ListView):

    model = Collection

