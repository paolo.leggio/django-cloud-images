from django.contrib import admin

from .models import Collection, Image
from django.utils.html import format_html


class ImageAdmin(admin.ModelAdmin):
    list_display = ('preview', 'file')
    exclude = (
        'focal_point_x',
        'focal_point_y',
        'focal_point_width',
        'focal_point_height',
    )

    def preview(self, obj):
        html = obj.file.image(width=200)
        return format_html(html)


class CollectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'images_preview')

    def images_preview(self, obj):
        html = ''.join([i.file.image(width=100 ) for i in obj.images.all()])
        return format_html(html)


admin.site.register(Image, ImageAdmin)
admin.site.register(Collection, CollectionAdmin)
