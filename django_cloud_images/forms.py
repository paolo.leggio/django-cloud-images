from sortedm2m.forms import SortedMultipleChoiceField, SortedCheckboxSelectMultiple


class SortedCheckboxSelectMultipleImage(SortedCheckboxSelectMultiple):
    pass


    # def render(self, name, value, attrs=None, choices=(), renderer=None):
    #     return ''


class SortedMultipleImagesField(SortedMultipleChoiceField):
    widget = SortedCheckboxSelectMultipleImage
