# -*- coding: utf-8
from django.apps import AppConfig, apps
from django.core.exceptions import ImproperlyConfigured
import os


class DjangoCloudImagesConfig(AppConfig):
    name = 'django_cloud_images'


    def ready(self):
        if not apps.is_installed('sortedm2m'):
            raise ImproperlyConfigured('django_cloud_images require "sortedm2m" in INSTALLED_APP')

        if not apps.is_installed('cloudinary'):
            raise ImproperlyConfigured('django_cloud_images require "cloudinary" in INSTALLED_APP')

        CLOUDINARY_URL = os.getenv('CLOUDINARY_URL', None)

        if not CLOUDINARY_URL:
            raise ImproperlyConfigured('Set CLOUDINARY_URL env variable for use django_cloud_images: '
                                       '"export CLOUDINARY_URL=cloudinary://467581854943958:PIGRvlyDdIR4-Ute3gXrQSRDYjY@dbuz2gc8i"')

