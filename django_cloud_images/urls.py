# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView

from . import views


app_name = 'django_cloud_images'
urlpatterns = [
    url(
        regex="^Image/~create/$",
        view=views.ImageCreateView.as_view(),
        name='Image_create',
    ),
    url(
        regex="^Image/(?P<pk>\d+)/~delete/$",
        view=views.ImageDeleteView.as_view(),
        name='Image_delete',
    ),
    url(
        regex="^Image/(?P<pk>\d+)/$",
        view=views.ImageDetailView.as_view(),
        name='Image_detail',
    ),
    url(
        regex="^Image/(?P<pk>\d+)/~update/$",
        view=views.ImageUpdateView.as_view(),
        name='Image_update',
    ),
    url(
        regex="^Image/$",
        view=views.ImageListView.as_view(),
        name='Image_list',
    ),
	url(
        regex="^Collection/~create/$",
        view=views.CollectionCreateView.as_view(),
        name='Collection_create',
    ),
    url(
        regex="^Collection/(?P<pk>\d+)/~delete/$",
        view=views.CollectionDeleteView.as_view(),
        name='Collection_delete',
    ),
    url(
        regex="^Collection/(?P<pk>\d+)/$",
        view=views.CollectionDetailView.as_view(),
        name='Collection_detail',
    ),
    url(
        regex="^Collection/(?P<pk>\d+)/~update/$",
        view=views.CollectionUpdateView.as_view(),
        name='Collection_update',
    ),
    url(
        regex="^Collection/$",
        view=views.CollectionListView.as_view(),
        name='Collection_list',
    ),
	]
