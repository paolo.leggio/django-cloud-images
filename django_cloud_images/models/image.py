# -*- coding: utf-8 -*-
import os.path

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from taggit.managers import TaggableManager
from ..fields.cloud import CloudImageField


class Image(TimeStampedModel):
    title = models.CharField(max_length=255, verbose_name=_('title'))
    file = CloudImageField(width_field='width', height_field='height', title_field='title')

    width = models.IntegerField(verbose_name=_('width'), editable=False, null=True, blank=True)
    height = models.IntegerField(verbose_name=_('height'), editable=False, null=True, blank=True)
    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True, db_index=True)
    uploaded_by_user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('uploaded by user'),
        null=True, blank=True, editable=False, on_delete=models.SET_NULL
    )

    # tags = TaggableManager(help_text=None, blank=True, verbose_name=_('tags'))

    focal_point_x = models.PositiveIntegerField(null=True, blank=True)
    focal_point_y = models.PositiveIntegerField(null=True, blank=True)
    focal_point_width = models.PositiveIntegerField(null=True, blank=True)
    focal_point_height = models.PositiveIntegerField(null=True, blank=True)

    file_size = models.PositiveIntegerField(null=True, editable=False)

    tags = TaggableManager(blank=True)

    """ Informative name for model """

    def __unicode__(self):
        try:
            public_id = self.file.public_id
        except AttributeError:
            public_id = ''
        return "Image <%s:%s>" % (self.title, public_id)
