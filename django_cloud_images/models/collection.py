# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django_cloud_images.fields.gallery import GalleryField


class Collection(TimeStampedModel):
    title = models.CharField(max_length=255, verbose_name=_('title'))
    description = models.TextField(verbose_name=_('Description'))
    images = GalleryField(blank=True)
