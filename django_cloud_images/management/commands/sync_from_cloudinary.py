# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django_cloud_images.models import Image
from cloudinary import uploader, api, CloudinaryResource
from django.core.cache import cache

CACHE_TIMEOUT = 60 * 60 * 24  # 24h
CLOUDINARY_MAX_SIZE = 10485760
images = list(Image.objects.all())
USE_CACHE = True


def only(source, *keys):
    return {key: source[key] for key in keys if key in source}


def omit(source, *keys):
    return {key: source[key] for key in source.keys() if key not in keys}


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill=u'█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s %s/%s\r' % (prefix, bar, percent, suffix, iteration, total))
    # Print New Line on Complete
    if iteration == total:
        print()


class Command(BaseCommand):
    _cloudinary_library_cache = None

    @property
    def cloudinary_library_cache(self):
        if not self._cloudinary_library_cache:
            self._cloudinary_library_cache = self._get_cloudinary_library()
        return self._cloudinary_library_cache

    def _get_cloudinary_library(self):
        cache_key = 'check_cloudinary_library'
        library = []
        if not cache.get(cache_key) or not USE_CACHE:
            print('Cache cloudinary library in memory')
            next_cursor = None
            while True:
                result = api.resources(context=True, next_cursor=next_cursor, tags=True, max_results=1000)
                library += result.get('resources', [])
                print('.')

                # break
                next_cursor = result.get('next_cursor', None)
                if next_cursor is None:
                    break

            cache.set(cache_key, library, CACHE_TIMEOUT)

            # pprint.pprint(self.cloudinary_library_cache)
        else:
            library = cache.get(cache_key)

        print('cached metadata of {} images from cloudinary'.format(len(library)))
        return library

    def handle(self, *args, **options):
        print('sync from cloudinary')

        all_images = list(Image.objects.all().prefetch_related('tags'))

        def get_image(id):
            return next((i for i in all_images if id == i.file.public_id), None)

        present = 0
        missing = 0
        iterate = 0
        for cloud_image in self.cloudinary_library_cache:
            iterate += 1
            printProgressBar(iterate, len(self.cloudinary_library_cache))
            image = get_image(cloud_image.get('public_id'))

            if image:
                print('OK')
                tags = [t.name for t in image.tags.all()]

                if str(image.file.version) != str(cloud_image.get('version')):
                    print('Version mismatch {} vs {}! Fix'.format(image.file.version, cloud_image.get('version')))
                    image = image
                    image.file.version = cloud_image.get('version')
                    image.save()

                if set(tags) != set(cloud_image.get('tags')):
                    print('tags mismetch: present: {}, expect:{}. Fix'.format(tags, cloud_image.get('tags')))
                    image.tags.add(*cloud_image.get('tags', []))
                present += 1
            else:
                missing += 1
                print('Image not exist in DB ({})'.format(cloud_image.get('public_id')))
                cloud_resource = CloudinaryResource(**only(cloud_image, *['public_id', 'format', 'version', 'signature',
                                                                          'url_options', 'metadata', 'type',
                                                                          'resource_type', 'default_resource_type']))

                di = Image()
                di.title = cloud_image.get('context', {}).get('custom', {}).get('caption', None) or cloud_image.get(
                    'public_id')
                setattr(di, 'file', cloud_resource)
                setattr(di, 'width', cloud_image.get('width'))
                setattr(di, 'height', cloud_image.get('height'))
                di.save()
                di.tags.add(*cloud_image.get('tags', []))

        print('{} missing images - {} already present'.format(missing, present))
