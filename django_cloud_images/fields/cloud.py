from django.utils import six
from cloudinary.models import CloudinaryField, uploader
import warnings
from django.core.validators import URLValidator
url = URLValidator()


class CloudImageField(CloudinaryField):
    def __init__(self, *args, **kwargs):
        self.title_field = kwargs.pop("title_field", None)
        super(CloudImageField, self).__init__(*args, **kwargs)


    def pre_save(self, model_instance, add):

        value = super(CloudImageField, self).pre_save(model_instance, add)
        if isinstance(value, six.string_types):
            if not value:
                return None

            try:
                # Check if is url
                url(value)

                warnings.warn('check if strig is an image url or cloudinaryPath')

                # Todo: Check if is cloudinaryPath

                options = {"type": self.type, "resource_type": self.resource_type}
                # options.update(self.upload_options_with_filename(model_instance, value.name))
                instance_value = uploader.upload_resource(value, **options)
                setattr(model_instance, self.attname, instance_value)
                if self.width_field:
                    setattr(model_instance, self.width_field, instance_value.metadata.get('width'))
                if self.height_field:
                    setattr(model_instance, self.height_field, instance_value.metadata.get('height'))
                return self.get_prep_value(instance_value)
            except:
                return value
        else:
            return value

    def upload_options(self, model_instance):
        options = {}
        context = {}

        if self.title_field:
            title = getattr(model_instance, self.title_field)
            options['public_id'] = title
            context['caption'] = title

        options['context'] = context
        # return {
        #     # 'public_id' : 'my_folder/my_name',
        #     # 'use_filename': True
        # }
        return options
