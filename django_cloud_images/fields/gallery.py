# -*- coding: utf-8 -*-

import warnings
from django.utils.functional import cached_property
from django.utils import six

from ..models.image import Image
# from models.collection import Collection
from ..utils import get_model_label
from sortedm2m.fields import SortedManyToManyField
from sortedm2m.forms import SortedMultipleChoiceField
from ..forms import SortedMultipleImagesField
from sortedm2m.fields import create_sorted_many_related_manager as orig_create_sorted_many_related_manager, get_rel, get_rel_to


def create_sorted_many_related_manager(superclass, rel, *args, **kwargs):
    klass = orig_create_sorted_many_related_manager(superclass, rel, *args, **kwargs)

    if issubclass(rel.model, Image):
        class SortedManyRelatedImageManager(klass):
            def _add_items(self, source_field_name, target_field_name, *objs, through_defaults=None):
                """
                For each object in 'objs' check if it is a string that can be used to automatically generate an image model.
                Used to be able to populate the field from a list of urls
                Example:
                    post.gallery = ['http://via.placeholder.com/100x100', 'http://via.placeholder.com/200x200']
                """
                new_objs = []
                if objs:
                    for obj in objs:
                        if isinstance(obj, six.string_types):
                            image = Image.objects.create(file=obj)
                            new_objs.append(image)
                        else:
                            new_objs.append(obj)
                super(SortedManyRelatedImageManager, self)._add_items(source_field_name, target_field_name, *new_objs)

        return SortedManyRelatedImageManager
    else:
        return klass


try:
    from django.db.models.fields.related_descriptors import ManyToManyDescriptor

    class GallerySortedManyToManyDescriptor(ManyToManyDescriptor):
        def __init__(self, field):
            super(GallerySortedManyToManyDescriptor, self).__init__(field.remote_field)

        @cached_property
        def related_manager_cls(self):
            model = self.rel.model
            return create_sorted_many_related_manager(
                model._default_manager.__class__,
                self.rel,
                # This is the new `reverse` argument (which ironically should
                # be False)
                reverse=False,
            )
except ImportError:
    # Django 1.8 support
    from django.db.models.fields.related import ReverseManyRelatedObjectsDescriptor

    class GallerySortedManyToManyDescriptor(ReverseManyRelatedObjectsDescriptor):
        @cached_property
        def related_manager_cls(self):
            return create_sorted_many_related_manager(
                get_rel_to(self.field)._default_manager.__class__,
                get_rel(self.field))


class GalleryField(SortedManyToManyField):
    default_model_class = Image
    default_form_class = SortedMultipleImagesField
    # default_through_class = BaseThroughClass

    def __init__(self, help_text=None, **kwargs):

        default_to = get_model_label(self.default_model_class)
        if "to" in kwargs.keys():
            to = kwargs.pop("to")
            old_to = get_model_label(to)

            if old_to != default_to:
                msg = "%s can only be a ForeignKey to %s; %s passed" % (
                    self.__class__.__name__, default_to, old_to
                )
                warnings.warn(msg, SyntaxWarning)

        kwargs['to'] = default_to
        kwargs['help_text'] = help_text
        # kwargs['base_class'] = self.default_through_class
        super(GalleryField, self).__init__( **kwargs)

    def formfield(self, **kwargs):
        # This is a fairly standard way to set up some defaults
        # while letting the caller override them.
        defaults = {
            'form_class': self.default_form_class,
            # 'rel': self.rel,
        }
        defaults.update(kwargs)
        return super(GalleryField, self).formfield(**defaults)

    def contribute_to_class(self, cls, name, **kwargs):
        super(GalleryField, self).contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, GallerySortedManyToManyDescriptor(self))




#
#
# class ImageField(models.ForeignKey):
#     # default_form_class = AdminImageFormField
#     default_model_class = Image
#
#     def __init__(self, **kwargs):
#         # We hard-code the `to` argument for ForeignKey.__init__
#         dfl = get_model_label(self.default_model_class)
#         if "to" in kwargs.keys():  # pragma: no cover
#             old_to = get_model_label(kwargs.pop("to"))
#             if old_to != dfl:
#                 msg = "%s can only be a ForeignKey to %s; %s passed" % (
#                     self.__class__.__name__, dfl, old_to
#                 )
#                 warnings.warn(msg, SyntaxWarning)
#         kwargs['to'] = dfl
#         super(ImageField, self).__init__(**kwargs)
#
#     def formfield(self, **kwargs):
#         # This is a fairly standard way to set up some defaults
#         # while letting the caller override them.
#         defaults = {
#             'form_class': self.default_form_class,
#             'rel': self.rel,
#         }
#         defaults.update(kwargs)
#         return super(ImageField, self).formfield(**defaults)
#
#
# class CollectionField(models.ForeignKey):
#     # default_form_class = AdminCollectionFormField
#     default_model_class = Collection
#
#     def __init__(self, **kwargs):
#         # We hard-code the `to` argument for ForeignKey.__init__
#         dfl = get_model_label(self.default_model_class)
#         if "to" in kwargs.keys():  # pragma: no cover
#             old_to = get_model_label(kwargs.pop("to"))
#             if old_to != dfl:
#                 msg = "%s can only be a ForeignKey to %s; %s passed" % (
#                     self.__class__.__name__, dfl, old_to
#                 )
#                 warnings.warn(msg, SyntaxWarning)
#         kwargs['to'] = dfl
#         super(CollectionField, self).__init__(**kwargs)
#
#     def formfield(self, **kwargs):
#         # This is a fairly standard way to set up some defaults
#         # while letting the caller override them.
#         defaults = {
#             'form_class': self.default_form_class,
#             'rel': self.rel,
#         }
#         defaults.update(kwargs)
#         return super(CollectionField, self).formfield(**defaults)
#
