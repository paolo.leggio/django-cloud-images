=============================
Django Cloud Images
=============================

.. image:: https://badge.fury.io/py/django-cloud-images.svg
    :target: https://badge.fury.io/py/django-cloud-images

.. image:: https://travis-ci.org/yourname/django-cloud-images.svg?branch=master
    :target: https://travis-ci.org/yourname/django-cloud-images

.. image:: https://codecov.io/gh/yourname/django-cloud-images/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/yourname/django-cloud-images

Your project description goes here

Documentation
-------------

The full documentation is at https://django-cloud-images.readthedocs.io.

Quickstart
----------

Install Django Cloud Images::

    pip install django-cloud-images

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_cloud_images.apps.DjangoCloudImagesConfig',
        ...
    )

Add Django Cloud Images's URL patterns:

.. code-block:: python

    from django_cloud_images import urls as django_cloud_images_urls


    urlpatterns = [
        ...
        url(r'^', include(django_cloud_images_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
