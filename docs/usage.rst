=====
Usage
=====

To use Django Cloud Images in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_cloud_images.apps.DjangoCloudImagesConfig',
        ...
    )

Add Django Cloud Images's URL patterns:

.. code-block:: python

    from django_cloud_images import urls as django_cloud_images_urls


    urlpatterns = [
        ...
        url(r'^', include(django_cloud_images_urls)),
        ...
    ]
