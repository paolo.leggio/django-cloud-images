============
Installation
============

At the command line::

    $ easy_install django-cloud-images

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-cloud-images
    $ pip install django-cloud-images
