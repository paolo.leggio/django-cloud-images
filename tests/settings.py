# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

import django
import os
import sys


sys.path.insert(0, os.path.join(os.getcwd(), 'tests'))


DEBUG = True
USE_TZ = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "o(mut9ftl%z!@)qhfym=al%34okm-cjt8tc)(08hvw79ex@5er"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django_cloud_images",
    "sample_app",
    "sortedm2m",
    "cloudinary",
    "taggit"
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()
