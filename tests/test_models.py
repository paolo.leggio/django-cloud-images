#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-cloud-images
------------

Tests for `django-cloud-images` models module.
"""

from django.test import TestCase

from django_cloud_images.models import Image, Collection
from sample_app.models import Post
from cloudinary import CloudinaryResource


def assert_all_is_images(list):
    for o in list:
        assert isinstance(o, Image)


class TestDjango_cloud_images(TestCase):

    def setUp(self):
        pass

    def test_something(self):
        pass

    def tearDown(self):
        pass

    def test_add_image_from_url(self):
        image = Image()
        image.file = 'http://via.placeholder.com/100x100'
        image.save()
        assert isinstance(image.file, CloudinaryResource)


    def test_gallery_field(self):
        image = Image()
        image.save()

        image2 = Image()
        image2.save()

        post = Post()
        post.save()
        post.gallery.set([image, image2])

        assert_all_is_images(post.gallery.all())

        assert list(post.gallery.all()) == [image, image2]
        post.gallery.set([image2, image])
        assert list(post.gallery.all()) == [image2, image]

    def test_gallery_field_by_urls(self):
        post = Post()
        post.save()
        post.gallery.set(['http://via.placeholder.com/100x100', 'http://via.placeholder.com/200x200'])
        assert_all_is_images(post.gallery.all())


    def test_image_model_save(self):
        image = Image()
        image.save()
        assert image.id

    def test_fake_model_save(self):
        post = Post()
        post.save()
        print(post)
        print(post.gallery)

        assert post.id
