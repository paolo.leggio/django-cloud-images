# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-28 10:13
from __future__ import unicode_literals

from django.db import migrations, models
import django_cloud_images.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('django_cloud_images', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Foo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=2)),
                ('gallery', django_cloud_images.fields.gallery.GalleryField(help_text=b'Foo', to='django_cloud_images.Image')),
            ],
        ),
    ]
