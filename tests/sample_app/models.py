

from django.db import models
from django_cloud_images.fields.gallery import GalleryField
from django_cloud_images.models import Image



class Foo(models.Model):
    title = models.CharField(max_length=2)


class Post(models.Model):

    title = models.CharField(max_length=255)

    gallery = GalleryField('Foo', related_name='gallery')

    # image = models.ForeignKey(Image, null=True)






