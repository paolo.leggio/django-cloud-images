from django.contrib import admin

from .models import Post, Foo

admin.site.register(Post)
admin.site.register(Foo)
